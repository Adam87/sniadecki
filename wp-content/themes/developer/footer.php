<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Developer
 */	

?> 
<?php if ( function_exists( 'get_field' ) && get_field( 'hide_footer' ) == 1 ) {} else { ?>
	
	<footer class="site-footer">
		
		<div role="contentinfo" class="content">
			
			<div class="row">
				<div class="col">
			<div class="footer-links"><div></div><?php if ( has_nav_menu( 'tertiary' ) ) { 
				wp_nav_menu( array('menu' => 'tertiary' ));
				} ?></div>
		<div class="site-info">
			<span class="site-info-text">
				<?php $copyright = get_theme_mod( 'copyright' ) ?>
			<?php printf( esc_html__( '%1$s', 'developer' ), $copyright ); ?>
			<span class="sep"> | </span>
			<?php $blog_title = get_bloginfo('name'); ?>
			<a href="<?php echo esc_attr(home_url()); ?>"><?php printf( esc_html__( '%1$s', 'developer' ), $blog_title ); ?></a>
		</span></div><!-- .site-info -->
		
	</div>
<div class="col textright">
			<h5><?php $textright = get_theme_mod( 'textright' ) ?>
		<?php printf( esc_html__( '%1$s', 'developer' ), $textright ); ?>
	</h5>
		</div>
		
		</div>
	</div>
		</footer>
		<?php } ?>
	<a href="#page" class="pg-developer-top">Top</a>

</div>	</div> </div>
<?php wp_footer();?>

</body>
</html>
