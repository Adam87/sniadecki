<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Developer
 */

get_header(); ?>
<div class="header-banner-top">
	
	  <div class="banner">
		
	
	    
		<div class="banner-image" style="background-image:url(<?php echo esc_url(get_header_image()); ?>);"></div>
			
	    <div class="pg-developer-header-overlay"></div>
	    <div class="primary-wrapper">
      
		<?php if ( have_posts() ) : ?>

			<span class="page-header">
				<h1 class="entry-title"><?php printf( esc_html__( 'Search Results for: %s', 'developer' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</span><!-- .page-header -->
	    </div>
    
	  </div>
	 
	</div>
	<div class="pg-developer-holder">

  <div class="content">
 	<div id="primary" class="content-area">
 		<main id="main" class="site-main">
 <div class="blog-grid row">
				
				
	

			

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
					/**
					 * Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called content-search.php and that will be used instead.
					 */
					get_template_part( 'template-parts/content', 'search' );
					?>

				<?php endwhile; ?>
</div>
	            <div class="pg-developer-content">
				<?php dv_paging_nav(); ?>
	            </div>

			<?php else : ?>
			  			<span class="page-header">
			  				<h1 class="entry-title"><?php printf( esc_html__( 'Search Results for: %s', 'developer' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			  			</span><!-- .page-header -->
			  	    </div>
    
			  	  </div>
	 
			  	</div>
			  	<div class="pg-developer-holder">

			    <div class="content">
			   	<div id="primary" class="content-area">
			   		<main id="main" class="site-main">

				<?php get_template_part( 'template-parts/content', 'none' ); ?>

			<?php endif; ?>

			</main><!-- #main -->
		</div><!-- #primary -->

	<?php get_sidebar(); ?>
</div></div>
	<?php get_footer(); ?>
	