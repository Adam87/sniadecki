<?php
/**
 * Developer functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Developer
 */

if ( ! function_exists( 'pg_developer_setup' ) ) :

function pg_developer_setup() {
	add_image_size('pg-developer-blog', 800, 500, true);
	add_image_size('pg-developer-grid', 600, 400, true);
	add_image_size('pg-developer-split', '', 600, true);
	add_image_size('pg-developer-minigal', 2000, 1400, true);
	add_image_size('pg-developer-testimonial', '', 500, true);
	add_image_size('pg-developer-timeline', 400, 400, true);
	
/*--------------------------*
/* Make Theme Translatable
/*--------------------------*/
load_theme_textdomain( 'developer', get_template_directory() . '/languages' );

/*--------------------------*
/* Add default posts & rss feed links to head
/*--------------------------*/
add_theme_support( 'automatic-feed-links' );

/*--------------------------*
/* Titles
/*--------------------------*/
add_theme_support( 'title-tag' );

/*--------------------------*
/* Post Thumbnails
/*--------------------------*/
add_theme_support( 'post-thumbnails' );

/*--------------------------*
/* Register Menus
/*--------------------------*/
register_nav_menus( array(
	'primary' => esc_html__( 'Primary Menu', 'developer' ),
	'secondary' => esc_html__( 'Secondary Menu', 'developer' ),
	'tertiary' => esc_html__( 'Footer Menu', 'developer' ),
) );

/*--------------------------*
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 /*--------------------------*/
add_theme_support( 'html5', array(
	'search-form',
	'comment-form',
	'comment-list',
	'gallery',
	'caption',
) );

/*--------------------------*
 * Enable support for Post Formats.
/*--------------------------*/
add_theme_support( 'post-formats', array(
	'gallery',
) );

add_editor_style( 'custom-editor-style.css' );

/*--------------------------*/
// Set up the WordPress core custom background feature.
/*--------------------------*/
add_theme_support( 'custom-background', apply_filters( 'pg_developer_custom_background_args', array(
	'default-color' => 'ffffff',
	'default-image' => '',
) ) );
}
endif; // pg_developer_setup
add_action( 'after_setup_theme', 'pg_developer_setup' );

/*--------------------------*
* Set the content width in pixels, based on the theme's design and stylesheet.
*
* Priority 0 to make it available to lower priority callbacks.
*
* @global int $content_width
/*--------------------------*/
function pg_developer_content_width() {
$GLOBALS['content_width'] = apply_filters( 'pg_developer_content_width', 640 );
}
add_action( 'after_setup_theme', 'pg_developer_content_width', 0 );

/*--------------------------*
/* Register Widget Area
/*--------------------------*/
function pg_developer_widgets_init() {
register_sidebar( array(
	'name'          => esc_html__( 'Sidebar', 'developer' ),
	'id'            => 'sidebar-1',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget'  => '</aside>',
	'before_title'  => '<h2 class="widget-title">',
	'after_title'   => '</h2>',
) );
}
add_action( 'widgets_init', 'pg_developer_widgets_init' );

/*--------------------------*
/* Register Fonts
/*--------------------------*/
function pg_developer_fonts_url() {
$font_url = '';

/*
Translators: If there are characters in your language that are not supported
by chosen font(s), translate this to 'off'. Do not translate into your own language.
 */
if ( 'off' !== _x( 'on', 'Google font: on or off', 'developer' ) ) {
    $font_url = add_query_arg( 'family', urlencode( 'Marck Script|Open Sans:400italic,400,700,300,600&subset=latin,latin-ext' ), "//fonts.googleapis.com/css" );
}
return $font_url;
}

/*--------------------------*
/* Enqueue Scripts & Styles
/*--------------------------*/
function pg_developer_scripts() {
wp_enqueue_style( 'pg-developer-style', get_stylesheet_uri() );
wp_enqueue_style('font-awesome', get_template_directory_uri() . "/css/font-awesome.min.css", 'screen');
wp_enqueue_script( 'dynamics', get_template_directory_uri() . '/js/dynamics.min.js', array(), '20120206', true );
wp_enqueue_script( 'pg-developer-main', get_template_directory_uri() . '/js/main.js', array('jquery'), '20150708', true );
$site_parameters = array(
    'site_url' => get_site_url(),
    'theme_directory' => get_template_directory_uri()
    );
wp_localize_script( 'pg-developer-main', 'SiteParameters', $site_parameters );

if ( is_home() || is_archive() || is_search() ) {
wp_enqueue_script( 'masonry' );
}
wp_enqueue_script( 'skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	wp_enqueue_script( 'comment-reply' );
}

 wp_enqueue_style( 'pg-developer-fonts', pg_developer_fonts_url(), array(), '1.0.0' );
        
}

add_action( 'wp_enqueue_scripts', 'pg_developer_scripts' );

/*--------------------------*
/* Custom Header
/*--------------------------*/
require get_template_directory() . '/inc/custom-header.php';

/*--------------------------*
/* Template Tags
/*--------------------------*/
require get_template_directory() . '/inc/template-tags.php';

/*--------------------------*
/* Cusotm Independent Functions
/*--------------------------*/
require get_template_directory() . '/inc/extras.php';

/*--------------------------*
/* Customizer
/*--------------------------*/
require get_template_directory() . '/inc/customizer.php';

/*--------------------------*
/* Jetpack Compatibility
/*--------------------------*/
require get_template_directory() . '/inc/jetpack.php';

/*--------------------------*
/* Add Custom Fields
/*--------------------------*/
$value= '';
require get_template_directory() . '/inc/custom-fields.php';

/*--------------------------*
/* Page Builder Contact Form Select
/*--------------------------*/

function pg_developer_acf_load_contact_field( $field )
{
	// Reset choices
	$field['choices'] = array();
 
	$post_type = 'wpcf7_contact_form';
	
	$args = array (
		'post_type' => $post_type,
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC',
	);
	
	$contact_forms = get_posts($args);
	
	$choices = array();
	foreach( $contact_forms as $form) {
		$choices[$form->ID] = strip_tags($form->post_title);
	}
	// loop through array and add to field 'choices'
	if( is_array($choices) )
	{
		$field['choices']['none'] = 'none';
		foreach( $choices as $choice )
		{
			$field['choices'][ $choice ] = $choice;
		}
	}
 
    // Important: return the field
    return $field;
}
 
// v4.0.0 and above
add_filter('acf/load_field/name=dv_contact', 'pg_developer_acf_load_contact_field');
add_filter('acf/load_field/name=dv_form', 'pg_developer_acf_load_contact_field');
add_filter('acf/load_field/name=dv_select_form', 'pg_developer_acf_load_contact_field');

if (class_exists('WP_Customize_Control')) {
    class WP_Customize_Category_Control extends WP_Customize_Control {
        /**
         * Render the control's content.
         *
         * @since 3.4.0
         */
        public function render_content() {
            $dropdown = wp_dropdown_categories(
                array(
                    'name'              => '_customize-dropdown-categories-' . $this->id,
                    'echo'              => 0,
                    'show_option_none'  => esc_html__( '&mdash; Select &mdash;','developer' ),
                    'option_none_value' => '0',
                    'selected'          => $this->value(),
                )
            );
 
            
            $dropdown = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
 
            printf(
                '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
                $this->label,
                $dropdown
            );
        }
    }
}

/*--------------------------*
/* Map API
/*--------------------------*/

function dv_acf_init() {
	$dv_map_api = get_theme_mod('gmap');
	acf_update_setting('google_api_key', $dv_map_api);
}

add_action('acf/init', 'dv_acf_init');

/*--------------------------*
/* Dynamic sidebars
/*--------------------------*/

function pg_developer_load_sidebar( $field )
{
 // reset choices
 $field['choices'] = array();
 $field['choices']['none'] = 'Show Sidebar';
 $field['choices']['default'] = 'Fill Width';
 // load repeater from the options page

 
 $label = get_sub_field('sidebar_name');
 $value = str_replace(" ", "-", $label);
 $value = strtolower($value);
 
$field['choices'][ $value ] = $label;
 

 
 // Important: return the field
 return $field;
}
 
add_filter('acf/load_field/name=select_a_sidebar', 'pg_developer_load_sidebar');

/*--------------------------*
 * Add Author Links
*--------------------------*/
function pg_developer_author_profile( $contactmethods ) {
	
	$contactmethods['rss_url'] = 'RSS URL';
	$contactmethods['google_profile'] = 'Google Profile URL';
	$contactmethods['twitter_profile'] = 'Twitter Profile URL';
	$contactmethods['facebook_profile'] = 'Facebook Profile URL';
	$contactmethods['linkedin_profile'] = 'Linkedin Profile URL';
	
	return $contactmethods;
}
add_filter( 'user_contactmethods', 'pg_developer_author_profile', 10, 1);

/*--------------------------*
/* Plugins
/*--------------------------*/
require get_template_directory() . '/inc/plugins/plugin.php';
/*--------------------------*
/* Admin Styles
/*--------------------------*/
function pg_developer_customizer_css()
{
	wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/css/custom-style.css' );
	
   require_once get_template_directory() . '/inc/custom-styles.php';


wp_add_inline_style( 'custom-style', $custom_inline_style );

}
	
add_action( 'wp_enqueue_scripts', 'pg_developer_customizer_css');

function pg_developer_admin_css() {
    wp_enqueue_style('pg-developer-admin-style', get_template_directory_uri() . '/css/admin-style.css');
}
add_action('admin_enqueue_scripts', 'pg_developer_admin_css');
/*--------------------------*
/* Excerpt Length
/*--------------------------*/

function pg_developer_excerpt_length( $length ) {
	return 25;
}
add_filter( 'excerpt_length', 'pg_developer_excerpt_length', 999 );

/*add_filter( 'oembed_dataparse', function( $return, $data, $url )
{
    // Target only Vimeo:
    if(
            is_object( $data ) 
        &&  property_exists( $data, 'provider_name' )
        &&  'Vimeo' === $data->provider_name
    )
    {
        // Remove the unwanted attributes:
        $return = str_ireplace(
            array( 
                'frameborder="0"', 
                'webkitallowfullscreen', 
                'mozallowfullscreen' 
            ),
            '',
            $return
        );
    }
    return $return;
}, 10, 3 );*/

function pg_developer_wpcf7_ajax_loader () {
    return  get_stylesheet_directory_uri() . '/images/pg-developer-loader.gif';
}
add_filter('wpcf7_ajax_loader', 'pg_developer_wpcf7_ajax_loader');

/*--------------------------*
/* Tag Cloud Font Size
/*--------------------------*/

function pg_developer_tag_cloud_widget($args) {
    $args['largest'] = 12; //largest tag
    $args['smallest'] = 12; //smallest tag
    $args['unit'] = 'px'; //tag font unit
    return $args;
}
add_filter( 'widget_tag_cloud_args', 'pg_developer_tag_cloud_widget' );

/*--------------------------*
/* Yoast SEO Extension
/*--------------------------*/

function developer_yoastseo_acf() {
	wp_enqueue_script( 'acf_yoastseo', get_template_directory_uri() . '/js/acf_yoastseo.js', 'jquery' );
}

add_action( 'admin_init', 'developer_yoastseo_acf' );

