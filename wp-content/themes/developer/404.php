<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Developer
 */

get_header(); ?>

	<div class="header-banner-top">
		
		  <div class="banner">
			
			<div class="banner-image" style="background-image:url(<?php echo esc_url(get_header_image()); ?>);"></div>
				
		    <div class="pg-developer-header-overlay"></div>
		    <div class="primary-wrapper">
					<h1 class="entry-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'developer' ); ?></h1>
				  </div>
				
				</div>
 
					<div id="primary">
						<main id="main" class="site-main">
							
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'developer' ); ?></p>

					<?php get_search_form(); ?>

					

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
