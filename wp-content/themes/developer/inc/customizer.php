<?php
/**
 * developer Theme Customizer
 *
 * @package developer
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */

if ( ! class_exists( 'WP_Customize_Control' ) )
    return NULL;

/**
 * Class to create a custom post type control
 */

//function dv_sanitize_text( $input ) {
//    return wp_kses_post( force_balance_tags( $input ) );
//}

function dv_customize_register( $wp_customize ) {
	
	$wp_customize->add_section( 'dv_logo', array(
	    'priority'       => 10,
	    'capability'     => 'edit_theme_options',
	    'theme_supports' => '',
	    'title'          => 'Logo',
	    'description'    => '',
	) );

	   //Logo Image
	   $wp_customize->add_setting( 'logo_image', array( 
	   	'sanitize_callback' => 'esc_url_raw',
	   ) );

	   $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_image', array(
	   	'label' => esc_html__( 'Logo Upload', 'developer' ),
	   	'section' => 'dv_logo',
	   	'settings' => 'logo_image'
	
	   ) ) );
		$wp_customize->add_section( 'dv_preloader', array(
		    'priority'       => 11,
		    'capability'     => 'edit_theme_options',
		    'theme_supports' => '',
		    'title'          => 'Preloader',
		    'description'    => '',
		) );
		$wp_customize->add_setting( 'show_preloader', array(
		'default'        => false,
		 ) );

		$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'show_preloader', array(
		    'section'   => 'dv_preloader',
		    'label'     => 'Show page preloader graphic?',
		    'type'      => 'checkbox',
			'settings'   => 'show_preloader'	
		     ) )
		 );
	
	   	//Highlight Color
	     $wp_customize->add_setting( 'highlight_color', array(
	   	'default' => '#3997ab',
	   	'sanitize_callback' => 'sanitize_hex_color',
	     ) );
	

	
	   	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'highlight_color', array(
	   		'label'   => esc_html__( 'Site Colour', 'developer' ),
	   		'section' => 'colors',
	   		'settings'   => 'highlight_color'
		
	   	) ) );
			
			   	//Footer Color
			     $wp_customize->add_setting( 'footer_color', array(
			   	'default' => '#161b1f',
			   	'sanitize_callback' => 'sanitize_hex_color',
			   	) );
	

	
			   	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_color', array(
			   		'label'   => esc_html__( 'Footer Background Colour', 'developer' ),
			   		'section' => 'colors',
			   		'settings'   => 'footer_color'
		
			   	) ) );
					
					   	//Text Color Footer
					     $wp_customize->add_setting( 'text_color_footer', array(
					   	'default' => '#fff',
					   	'sanitize_callback' => 'sanitize_hex_color',
					   	) );
	

	
					   	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'text_color_footer', array(
					   		'label'   => esc_html__( 'Footer Text Colour', 'developer' ),
					   		'section' => 'colors',
					   		'settings'   => 'text_color_footer'
		
					   	) ) );
							
				   	     $wp_customize->add_setting( 'preloader_color', array(
				   	   	'default' => '#3997ab',
				   	   	'sanitize_callback' => 'sanitize_hex_color',
				   	     ) );
	

	
				   	   	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'preloader_color', array(
				   	   		'label'   => esc_html__( 'Preloader Colour', 'developer' ),
				   	   		'section' => 'colors',
				   	   		'settings'   => 'preloader_color'
		
				   	   	) ) );
							
							
							$wp_customize->add_section( 'dv_footer_options', array(
							    'priority'       => 180,
							    'capability'     => 'edit_theme_options',
							    'theme_supports' => '',
							    'title'          => 'Footer',
							    'description'    => '',
							) );
	
							//Copyright
						  $wp_customize->add_setting( 'copyright', array(
							  'default' => 'All Rights Reserved.',
							  'sanitize_callback' => 'esc_attr',
							) );
	

	
							$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'copyright', array(
								'label'   => esc_html__( 'Copyright', 'developer' ),
								'section' => 'dv_footer_options',
								'settings'   => 'copyright',
				
							    )));
								
								//Right Text - Footer
							  $wp_customize->add_setting( 'textright', array(
								  'default' => 'Where dreams homes begin...',
								  'sanitize_callback' => 'esc_attr',
								) );
	

	
								$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'textright', array(
									'label'   => esc_html__( 'Text right', 'developer' ),
									'section' => 'dv_footer_options',
									'settings'   => 'textright',
				
								    )));
				
				
								//Social Settings
								$wp_customize->add_section( 'dv_social_options', array(
								    'priority'       => 170,
								    'capability'     => 'edit_theme_options',
								    'theme_supports' => '',
								    'title'          => 'Social Links',
								    'description'    => '',
								) );
	
								$wp_customize->add_setting( 'twitter_text', array(
									'default' => '',
									'sanitize_callback' => 'esc_url_raw',
									 'priority' => 1
								) );
	
								$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'twitter_text', array(
									'label'   => esc_html__( 'Twitter url', 'developer' ),
									'section' => 'dv_social_options',
									'settings'   => 'twitter_text',
		
								) ) );
	
	
								$wp_customize->add_setting( 'facebook_text', array(
									'default' => '',
									'sanitize_callback' => 'esc_url_raw',
									 'priority' => 2
								) );
	
								$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'facebook_text', array(
									'label'   => esc_html__( 'Facebook url', 'developer' ),
									'section' => 'dv_social_options',
									'settings'   => 'facebook_text',
		
								) ) );
	
								$wp_customize->add_setting( 'pinterest_text', array(
									'default' => '',
									'sanitize_callback' => 'esc_url_raw',
									 'priority' => 3
								) );
	
								$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'pinterest_text', array(
									'label'   => esc_html__( 'Pinterest url', 'developer' ),
									'section' => 'dv_social_options',
									'settings'   => 'pinterest_text',
		
								) ) );
	
								$wp_customize->add_setting( 'google1_text', array(
									'default' => '',
									'sanitize_callback' => 'esc_url_raw',
									 'priority' => 4
								) );
	
	
								$wp_customize->add_setting( 'dribbble_text', array(
									'default' => '',
									'sanitize_callback' => 'esc_url_raw',
									 'priority' => 5
								) );
	
								$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'dribbble_text', array(
									'label'   => esc_html__( 'Dribbble url', 'developer' ),
									'section' => 'dv_social_options',
									'settings'   => 'dribbble_text',
		
								) ) );
	
								$wp_customize->add_setting( 'flickr_text', array(
									'default' => '',
									'sanitize_callback' => 'esc_url_raw',
									 'priority' => 6
								) );
	
								$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'flickr_text', array(
									'label'   => esc_html__( 'Flickr url', 'developer' ),
									'section' => 'dv_social_options',
									'settings'   => 'flickr_text',
		
								) ) );
	
								$wp_customize->add_setting( 'youtube_text', array(
									'default' => '',
									'sanitize_callback' => 'esc_url_raw',
									 'priority' => 7
								) );
	
								$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'youtube_text', array(
									'label'   => esc_html__( 'YouTube url', 'developer' ),
									'section' => 'dv_social_options',
									'settings'   => 'youtube_text',
		
								) ) );
	
								$wp_customize->add_setting( 'instagram_text', array(
									'default' => '',
									'sanitize_callback' => 'esc_url_raw',
									 'priority' => 8
								) );
	
								$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'instagram_text', array(
									'label'   => esc_html__( 'Instagram url', 'developer' ),
									'section' => 'dv_social_options',
									'settings'   => 'instagram_text',
		
								) ) );
	
								$wp_customize->add_setting( 'vimeo_text', array(
									'default' => '',
									'sanitize_callback' => 'esc_url_raw',
									 'priority' => 9
								) );
	
								$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'vimeo_text', array(
									'label'   => esc_html__( 'Vimeo url', 'developer' ),
									'section' => 'dv_social_options',
									'settings'   => 'vimeo_text',
		
								) ) );
	
								$wp_customize->add_setting( 'behance_text', array(
									'default' => '',
									'sanitize_callback' => 'esc_url_raw',
									 'priority' => 10
								) );
	
								$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'behance_text', array(
									'label'   => esc_html__( 'Behance url', 'developer' ),
									'section' => 'dv_social_options',
									'settings'   => 'behance_text',
		
								) ) );
	
								$wp_customize->add_setting( 'linkedin_text', array(
									'default' => '',
									'sanitize_callback' => 'esc_url_raw',
									 'priority' => 1
								) );
	
								$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'linkedin_text', array(
									'label'   => esc_html__( 'LinkedIn url', 'developer' ),
									'section' => 'dv_social_options',
									'settings'   => 'linkedin_text',
		
								) ) );
								
								//Google Maps
								
								$wp_customize->add_section( 'dv_map_options', array(
								    'priority'       => 190,
								    'capability'     => 'edit_theme_options',
								    'theme_supports' => '',
								    'title'          => 'Google Maps',
								    'description'    => '',
								) );
	
								
							  $wp_customize->add_setting( 'gmap', array(
								  'default' => '',
								  'sanitize_callback' => 'esc_attr',
								) );
	

	
								$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'gmap', array(
									'label'   => esc_html__( 'Map API', 'developer' ),
									'section' => 'dv_map_options',
									'settings'   => 'gmap',
				
								    )));
								
									
	

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->remove_control( 'header_textcolor');
	$wp_customize->remove_section( 'background_image');
	$wp_customize->remove_control( 'background_color');
}
add_action( 'customize_register', 'dv_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function dv_customize_preview_js() {
	wp_enqueue_script( 'dv_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'dv_customize_preview_js' );
