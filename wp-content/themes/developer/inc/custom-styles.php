 <?php
$dv_color = esc_attr(get_theme_mod('highlight_color'));
$dv_footer = esc_attr(get_theme_mod('footer_color'));
$dv_text_footer = esc_attr(get_theme_mod('text_color_footer'));
$dv_preloader = esc_attr(get_theme_mod('preloader_color'));
function hex2rgb($hex) {
$hex = str_replace("#", "", $hex);

if(strlen($hex) == 3) {
$r = hexdec(substr($hex,0,1).substr($hex,0,1));
$g = hexdec(substr($hex,1,1).substr($hex,1,1));
$b = hexdec(substr($hex,2,1).substr($hex,2,1));
} else {
$r = hexdec(substr($hex,0,2));
$g = hexdec(substr($hex,2,2));
$b = hexdec(substr($hex,4,2));
}
$rgb = array($r, $g, $b);

return $rgb; // returns an array with the rgb values
}
$RGB_color = hex2rgb($dv_color);
$RGB_loader_color = hex2rgb($dv_preloader);
$rgb_color = implode(", ", $RGB_color);
$rgb_loader_color = implode(", ", $RGB_loader_color);
$custom_inline_style = '.site-footer, .site-footer .content{background-color:'.$dv_footer.';}.site-footer .footer-links a{color:'.$dv_text_footer.';}#pg-developer-banner > div a:hover, .widget ul a:hover, a, a:visited, a:hover, a:focus, a:active, .statistic-list .box .icon, .pg-developer-input input:focus{color:'.$dv_color.';}.paging .active a, .paging a:hover, input.wpcf7-form-control:focus, textarea:focus, textarea.wpcf7-form-control:focus, #pg-developer-banner > div a, .nav a:hover, .tagcloud a:hover,
#sidebar .htagcloud a:hover{border-color:'.$dv_color.';}.blog-grid article.post .entry-footer .comments-link a:hover, .slide ol li, .paging .active a, .paging a:hover, .wpcf7-submit, .wpcf7-form input[type="submit"],.comment-form .submit, .btn-link, .error-404 .search-submit, .cd-top, .pg-developer-single-point > a, button.pg-developer-highlight, .timeline li::before, .timeline li::after, .pg-developer-single-point > a, .pg-developer-expand__card, .pg-developer-single-point.is-open > a, #pg-developer-menu-trigger .pg-developer-menu-icon, button:hover,
input[type="button"]:hover,
input[type="reset"]:hover,
input[type="submit"]:hover,
.bton-link:hover, .pg-developer-input input:focus + label::after, .tagcloud a:hover,
#sidebar .htagcloud a:hover, .widget_most_commented_widget li span, .timeline__date {background-color:'.$dv_color.';}
.pg-developer-top{background-color:rgba('.$rgb_color.', 0.8);}
.object{border:4px solid rgba('.$rgb_loader_color.',1);}
.pg-developer-input input + label span::after{background-image:linear-gradient(to right, '.$dv_color.' 50%, rgba(255, 255, 255, 0) 0%);!important}
@-webkit-keyframes pg-developer-pulse {
  0% {
    -webkit-transform: scale(1);
    box-shadow: inset 0 0 1px 1px rgba('.$rgb_color.', 0.8);
  }
  50% {
    box-shadow: inset 0 0 1px 1px rgba('.$rgb_color.', 0.8);
  }
  100% {
    -webkit-transform: scale(1.6);
    box-shadow: inset 0 0 1px 1px rgba('.$rgb_color.', 0);
  }
}
@-moz-keyframes pg-developer-pulse {
  0% {
    -moz-transform: scale(1);
    box-shadow: inset 0 0 1px 1px rgba('.$rgb_color.', 0.8);
  }
  50% {
    box-shadow: inset 0 0 1px 1px rgba('.$rgb_color.', 0.8);
  }
  100% {
    -moz-transform: scale(1.6);
    box-shadow: inset 0 0 1px 1px rgba('.$rgb_color.', 0);
  }
}
@keyframes pg-developer-pulse {
  0% {
    -webkit-transform: scale(1);
    -moz-transform: scale(1);
    -ms-transform: scale(1);
    -o-transform: scale(1);
    transform: scale(1);
    box-shadow: inset 0 0 1px 1px rgba('.$rgb_color.', 0.8);
  }
  50% {
    box-shadow: inset 0 0 1px 1px rgba('.$rgb_color.', 0.8);
  }
  100% {
    -webkit-transform: scale(1.6);
    -moz-transform: scale(1.6);
    -ms-transform: scale(1.6);
    -o-transform: scale(1.6);
    transform: scale(1.6);
    box-shadow: inset 0 0 1px 1px rgba('.$rgb_color.', 0);
  }
}
';