<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Developer
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>
 <?php $dv_color  = get_theme_mod('highlight_color'); ?>
<body <?php body_class("pg-developer-site"); ?>>
	<?php if(true === get_theme_mod('show_preloader')){ ?>
	<div id="loading">
	<div id="loading-center">
	<div id="loading-center-absolute">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
	<rect fill="#fff" width="3" height="100" transform="translate(0) rotate(180 3 50)">
	  <animate
	      attributeName="height"
	      attributeType="XML"
	      dur="1s"
	      values="30; 100; 30"
	      repeatCount="indefinite"/>
	</rect>
	<rect x="17" fill="<?php echo esc_attr($dv_color);?>" width="3" height="100" transform="translate(0) rotate(180 20 50)">
	  <animate
	      attributeName="height"
	      attributeType="XML"
	      dur="1s"
	      values="30; 100; 30"
	      repeatCount="indefinite"
	      begin="0.1s"/>
	</rect>
	<rect x="40" fill="#fff" width="3" height="100" transform="translate(0) rotate(180 40 50)">
	  <animate
	      attributeName="height"
	      attributeType="XML"
	      dur="1s"
	      values="30; 100; 30"
	      repeatCount="indefinite"
	      begin="0.3s"/>
	</rect>
	<rect x="60" fill="<?php echo esc_attr($dv_color);?>" width="3" height="100" transform="translate(0) rotate(180 58 50)">
	  <animate
	      attributeName="height"
	      attributeType="XML"
	      dur="1s"
	      values="30; 100; 30"
	      repeatCount="indefinite"
	      begin="0.5s"/>
	</rect>
	<rect x="80" fill="#fff" width="3" height="100" transform="translate(0) rotate(180 76 50)">
	  <animate
	      attributeName="height"
	      attributeType="XML"
	      dur="1s"
	      values="30; 100; 30"
	      repeatCount="indefinite"
	      begin="0.1s"/>
	</rect>
	</svg>
	</div>
	</div>
 
	</div>
	<?php } ?>
	<div class="pg-developer-wrap">
<div id="page" class="hfeed site pg-developer-container">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'developer' ); ?></a>

	<nav id="pg-developer-lateral-nav">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu',  ) ); ?>
		<?php
		$twitter  = get_theme_mod('twitter_text');  
		$facebook  = get_theme_mod('facebook_text');  
		$googleplus  = get_theme_mod('google1_text');  
		$dribbble  = get_theme_mod('dribbble_text');  
		$pinterest  = get_theme_mod('pinterest_text');  
		$vimeo  = get_theme_mod('vimeo_text');  
		$tumblr  = get_theme_mod('tumblr_text');  
		$youtube  = get_theme_mod('youtube_text');  
		 $stackof  = get_theme_mod('stackof_text');  
		  $instagram  = get_theme_mod('instagram_text');  
		   $linkedin  = get_theme_mod('linkedin_text'); 
		?>
		                    <div class="bottom-area">
					   		<div class="socials">
					   				<ul>
					   									<?php if(!empty($facebook)){ ?>
					   									<li><a href="<?php echo esc_url($facebook);?>" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
					   									<?php } ?>
					   									<?php if(!empty($twitter)){ ?>
					   									<li><a href="<?php echo esc_url($twitter);?>" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
					   									<?php } ?>
					   									<?php if(!empty($vimeo)){ ?>
					   									<li><a href="<?php echo esc_url($vimeo);?>" target="_blank" class="vimeo"><i class="fa fa-vimeo-square"></i></a></li>
					   									<?php } ?>
					   									<?php if(!empty($behance)){ ?>
					   									<li><a href="<?php echo esc_url($behance);?>" target="_blank" class="behance"><i class="fa fa-behance-square"></i></a></li>
					   									<?php } ?>
					   									<?php if(!empty($dribbble)){ ?>
					   									<li><a href="<?php echo esc_url($dribbble);?>" target="_blank" class="dribbble"><i class="fa fa-dribbble"></i></a></li>
					   									<?php } ?>
					   									<?php if(!empty($pinterest)){ ?>
					   									<li><a href="<?php echo esc_url($pinterest);?>" target="_blank" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
					   									<?php } ?>
					   									<?php if(!empty($instagram)){ ?>
					   									<li><a href="<?php echo esc_url($instagram);?>" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a></li>
					   									<?php } ?>
					   									<?php if(!empty($flickr)){ ?>
					   									<li><a href="<?php echo esc_url($flickr);?>" target="_blank" class="flickr"><i class="fa fa-flickr"></i></a></li>
					   									<?php } ?>
					   									<?php if(!empty($linkedin)){ ?>
					   									<li><a href="<?php echo esc_url($linkedin);?>" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
					   									<?php } ?>
					   									<?php if(!empty($youtube)){ ?>
					   									<li><a href="<?php echo esc_url($youtube);?>" target="_blank" class="youtube"><i class="fa fa-youtube"></i></a></li>
					   									<?php } ?>
					   																			</ul>
					   			

		                       
								   			<?php
											if ( has_nav_menu( 'secondary' ) ) {
								     wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'menu-wrapper') ); 
									 }
								   ?>
		                      </div>
		                    </div>
		</nav>

    <header class="pg-developer-main">
		<div id="pg-developer-logo">
		 <?php $dv_logo  = get_theme_mod('logo_image'); 
          if(!empty($dv_logo)) { ?>
 	       <a href="<?php echo esc_attr(home_url()); ?>"> <img class="a" src="<?php echo esc_url($dv_logo);?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"> </a>
 	       <?php }
 	   		 else { ?>
		<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
		<?php } ?>
		</div>
		<a id="pg-developer-menu-trigger" href="#0"><span class="pg-developer-menu-text"><?php esc_html_e( 'Menu', 'developer' ); ?></span><span class="pg-developer-menu-icon"></span></a>
	</header>
	
	
	<div class="pg-developer-main-content">
		
