<?php
if (have_rows('pb_content')):
    // loop through the rows of Data
    while (have_rows('pb_content')):
        the_row();

        if (get_row_layout() == 'block_content'):
            ?>
            <section class="white-section br_editor" <?php if (get_sub_field('id')) { ?>
                         id="<?php echo esc_attr(get_sub_field('id')); ?>"
                     <?php } ?>>
                <div class="center-holder">
                    <?php echo get_sub_field('editor_content'); ?>
                </div>
            </section>
            <?php
        // COLUMNS
        elseif (get_row_layout() == 'column_content'):if (get_sub_field('columns')):
                ?>
                <section class="white-section br_editor" <?php if (get_sub_field('id')) { ?>
                             id="<?php echo esc_attr(get_sub_field('id')); ?>"
                <?php } ?>>
                    <div class="pg-developer-content pg-developer-cols">
                         <?php while (has_sub_field('columns')): ?>
                            <div class="pg-developer-col">
                            <?php echo get_sub_field('editor_columns'); ?>
                            </div>
                            <?php endwhile;
                        endif;
                        ?>
                </div>
            </section>
            <?php
        // GALLERY

        elseif (get_row_layout() == 'screenshots_content'):
            $pclass = '';
            if (get_sub_field('background') == 'image') {
                $pclass = 'parallax-section';
            }
            ?>
            <section class="<?php echo esc_attr($pclass); ?>" <?php if (get_sub_field('id')) { ?>
                         id="<?php echo esc_attr(get_sub_field('id')); ?>"
            <?php } ?> style="background-color:<?php echo esc_attr(get_sub_field('color')); ?>;<?php if (get_sub_field('background') == 'image') { ?>background-image:url('<?php echo esc_attr(get_sub_field('sc_image')); ?>');<?php } ?>">

                <div class="gallery-container">
                    <h2 style="color:<?php echo esc_attr(get_sub_field('font_color')); ?>;"><?php echo esc_attr(get_sub_field('title')); ?></h2>
                    <h4 style="color:<?php echo esc_attr(get_sub_field('subfont_color')); ?>;"><?php echo esc_attr(get_sub_field('subtitle')); ?></h4>
                    <div id="pg-developer-preview">
                        <div id="imgFull">
                            <img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/placehold.png" alt="<?php esc_html_e('full-image', 'developer'); ?>">
                        </div>
                    </div>
                    <div class="gallery">
            <?php
            $i = 0;


            $images = get_sub_field('screenshots');

            if ($images):
                $i = 0;
                foreach ($images as $image):
                    $i++;
                    $stutter = ($i * .2);
                    ?>
                                <img src="<?php echo esc_url($image['url']); ?>" width="225" alt="<?php echo esc_attr($image['alt']); ?>" data-sr="ease-in 20px, over .5s, wait <?php echo esc_attr($stutter) ?>s">

                            <?php endforeach; ?>
                        <?php endif; ?>


                    </div>
                </div>
            </section>

            <?php
        // TABS

        elseif (get_row_layout() == 'tabs_content'):
            $tab = get_sub_field('tab');

            if (get_sub_field('tab')):
                ?>
                <section <?php if (get_sub_field('id')) { ?>
                        id="<?php echo esc_attr(get_sub_field('id')); ?>"
                <?php } ?>>
                    <div class="pc-tab">
                        <input checked="checked" id="tab1" type="radio" name="pct" />
                        <input id="tab2" type="radio" name="pct" />
                        <input id="tab3" type="radio" name="pct" />
                        <input id="tab4" type="radio" name="pct" />
                        <input id="tab5" type="radio" name="pct" />
                        <input id="tab6" type="radio" name="pct" />
                        <input id="tab7" type="radio" name="pct" />
                        <input id="tab8" type="radio" name="pct" />

                        <nav>
                            <ul>
                <?php
                $count = 1;
                while (has_sub_field('tab')):
                    $tabstutter = ($count * .1);
                    if ($count == 1) : $tc = 'active';
                    else : $tc = '';
                    endif
                    ?>
                                    <li class="tab<?php echo esc_attr($count); ?>" data-sr="ease-in, over .5s, wait <?php echo $tabstutter ?>s"> <label for="tab<?php echo esc_attr($count); ?>"><?php echo esc_attr(get_sub_field('title')); ?></label></li>
                                    <?php
                                    $count++;
                                endwhile;
                                ?>
                            </ul>
                        </nav>
                        <?php endif;
                    ?>
                        <?php if (get_sub_field('tab')): ?>
                        <section>
                            <?php
                            $count = 1;
                            while (has_sub_field('tab')):
                                ?>


                                <div class="tab<?php echo esc_attr($count); ?>">
                    <?php echo get_sub_field('text'); ?>
                                </div>

                                <?php
                                $count++;
                            endwhile;
                            ?>
                        </section></div></section>
                <?php endif;
            ?>



            <?php
        // TIMELINE

        elseif (get_row_layout() == 'timeline'):


            if (get_sub_field('event')):
                ?>
                <section class="timeline" <?php if (get_sub_field('id')) { ?>
                             id="<?php echo esc_attr(get_sub_field('id')); ?>" <?php } ?> style="background-color:<?php echo esc_attr(get_sub_field('color')); ?>"
                         >

                    <ul>
                        <?php
                        while (has_sub_field('event')):
                            $event_img = get_sub_field('image');
                            ?>
                            <li data-sr="enter left move 46px scale up 20%, over 1s">
                                <?php if (get_sub_field('date')) { ?>
                                    <div class="timeline__date"><?php echo esc_attr(get_sub_field('date')); ?></div>
                                <?php } ?>
                                <img src="<?php echo esc_attr($event_img['sizes']['pg-developer-timeline']); ?>" alt="<?php echo esc_attr($event_img['alt']); ?>" /> 
                                <?php echo esc_attr(get_sub_field('event')); ?>
                            </li>
                            <?php endwhile;
                        ?>
                    </ul>

                    <?php endif;
                ?>

            </section>
            <?php
        // ACCORDION
        elseif (get_row_layout() == 'accordion_content'):if (get_sub_field('ac_tab')):
                ?>
                <section <?php if (get_sub_field('id')) { ?>
                        id="<?php echo esc_attr(get_sub_field('id')); ?>"
                    <?php } ?> class="white-section">
                    <ul class="accordion">
                        <?php while (has_sub_field('ac_tab')): ?>
                            <li>
                                <a><?php echo esc_attr(get_sub_field('title')); ?></a>

                                    <?php echo wp_kses_post(get_sub_field('text')); ?>

                            </li>
                                <?php endwhile;
                            ?>
                    </ul></section>
                        <?php endif;
                    ?>

            <?php
        // CONTACT

        elseif (get_row_layout() == 'contact_form'):
            $top = get_sub_field('padding_top');
            $bottom = get_sub_field('padding_bottom');
            ?>
            <section <?php if (get_sub_field('id')) { ?>
                    id="<?php echo esc_attr(get_sub_field('id')); ?>"
            <?php } ?> class="white-section" style="padding-top:<?php echo esc_attr($top); ?>px; padding-bottom:<?php echo esc_attr($bottom); ?>px;">
                <div class="dv-contact-form">
                    <h2><?php echo esc_attr(get_sub_field('title')); ?></h2>
                    <span><?php echo esc_attr(get_sub_field('subtitle')); ?></span>
            <?php
            $dv_form = get_sub_field('dv_select_form');

            echo do_shortcode('[contact-form-7 title="' . $dv_form . '"]');
            ?>


                </div>
            </section>
                    <?php
                // FEATURES
                elseif (get_row_layout() == 'stats_content'):if (get_sub_field('stat')):
                        ?>
                <article class="stat-section" <?php if (get_sub_field('id')) { ?>
                             id="<?php echo esc_attr(get_sub_field('id')); ?>"
                <?php } ?>>
                    <div class="pg-developer-content">
                        <ul class="statistic-list">
                <?php
                $i = 0;
                while (has_sub_field('stat')):

                    $stutter = ($i * .2)
                    ?>
                                <li data-sr="ease-in 50px, over 1s, wait <?php echo esc_attr($stutter); ?>s">
                                    <div class="box">

                                        <span class="icon fa <?php echo esc_attr(get_sub_field('icon')); ?>"></span>

                                    </div>
                                    <span class="description"><?php echo esc_attr(get_sub_field('title')); ?></span>
                                    <p><?php echo esc_attr(get_sub_field('text')); ?></p>
                                </li>
                                              <?php $i++;
                                          endwhile;
                                          ?>
                <?php endif;
            ?>
                    </ul>
                </div>
            </article>
                        <?php
                    //HERO HEADER

                    elseif (get_row_layout() == 'hero_header'):
                        ?>

            <section class="video-header big-background" id="video-bg">
                <div class="pattern"></div> 
                <div class="big-background-container">
                    <div class="logo"></div>
                    <h1 class="big-background-title"><?php echo esc_attr(get_sub_field('heading')); ?></h1>
                    <h2><?php echo esc_attr(get_sub_field('subheading')); ?></h2>
                    <div class="divider"></div>
                    <h3 class="big-background-subtitle"><?php echo esc_attr(get_sub_field('subheading2')); ?></h3>
                    <h5><?php echo esc_attr(get_sub_field('subheading3')); ?></h5>
            <?php
            if (get_sub_field('add_button')):
                while (has_sub_field('add_button')):
                    ?>
                    <?php if (get_sub_field('url')) { ?> 
                                <a class ="btn-link <?php if (get_sub_field('highlight')) { ?> pg-developer-highlight <?php } ?>" href="<?php echo esc_url(get_sub_field('url')); ?>"><?php echo esc_attr(get_sub_field('text')); ?> </a> <?php } else { ?>
                                <button class=" <?php if (get_sub_field('highlight')) { ?> pg-developer-highlight <?php } ?> <?php if (get_sub_field('dv_form') != 'none') { ?> open <?php } ?>"><?php echo esc_attr(get_sub_field('text')); ?> </button>
                    <?php }

                    if (get_sub_field('dv_form')) {
                        ?>
                                <div class="modal-frame">
                                    <div class="modal">
                                        <div class="modal-inset">
                                            <div class="button close"></div>

                                            <div class="modal-body">
                                    <?php
                                    $form = get_sub_field('dv_form');
                                    if ($form != 'none') {
                                        echo esc_attr($form);
                                        echo do_shortcode('[contact-form-7 title="' . $form . '"]');
                                        ?>
                        <?php } ?>  
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                            <?php } endwhile;
                                    endif;
                                    $image = get_sub_field('image'); ?>
                </div>
                                    <?php if (get_sub_field('video')): ?>

                    <video autoplay loop muted>
                <?php while (has_sub_field('video')): ?>
                            <source type="video/<?php echo esc_attr(get_sub_field('file_type')); ?>" src="<?php echo esc_url(get_sub_field('url')); ?>">

                <?php endwhile; ?>  
                    </video><img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="pg-developer-cover pg-developer-bgimg"/>
                    <?php endif;


                    if (!empty($image) && !(has_sub_field('video'))):
                        ?>

                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="pg-developer-cover"/>

                    <?php endif; ?>
            </section><div class="modal-overlay"></div>




            <?php
        //SPLIT BANNER

        elseif (get_row_layout() == 'split_banner'):
            $banner_img = get_sub_field('image');
            $imgsize = 'pg-developer-split';
            $imgsizebot = 'pg-developer-jumbotron';
            $imgvalues = get_sub_field('side');
            $identifier = rand(1, 299);
            $alignment = 'alignright';
            if (in_array("left", $imgvalues)):
                $alignment = 'alightleft'
                ?>
            <?php
            endif;
            $bottom = '';
            if (in_array("bottom", $imgvalues)) {
                $bottom = 'alignbottom';
            }
            $pclass = '';
            if (get_sub_field('background') == 'image') {
                $pclass = 'parallax-section';
            }
            ?>
            <section class="split-banner <?php echo esc_attr($pclass); ?>" <?php if (get_sub_field('id')) { ?>
                         id="<?php echo esc_attr(get_sub_field('id')); ?>"
            <?php } ?> style="background-color:<?php echo esc_attr(get_sub_field('color')); ?>;<?php if (get_sub_field('background') == 'image') { ?>background-image:url('<?php echo esc_attr(get_sub_field('sc_image')); ?>');<?php } ?>">

                <div class="pg-developer-content">
                    <div class="img-holder <?php echo esc_attr($alignment); ?>" data-sr="move 16px scale up 20%, over 2s">
            <?php echo wp_get_attachment_image($banner_img, $imgsize); ?>

                    </div>
                    <div class="text-holder" style="color:<?php echo esc_attr(get_sub_field('textfont_color')); ?>;">
                        <h2 style="color:<?php echo esc_attr(get_sub_field('font_color')); ?>;" ><?php echo esc_attr(get_sub_field('title')); ?></h2>
            <?php if (get_sub_field('sub_title')) : ?>
                            <h4 style="color:<?php echo esc_attr(get_sub_field('subfont_color')); ?>"><?php echo esc_attr(get_sub_field('sub_title')); ?></h4>
                        <?php endif; ?>
                        <?php echo wp_kses_post(get_sub_field('text')); ?>
            <?php if (get_sub_field('button_text')) :
                if (get_sub_field('video_link')) {
                    ?>
                                <a class="btn-link modal__trigger" data-modal="#pg-developer-modal-<?php echo esc_attr($identifier); ?>" href="" ><?php echo esc_attr(get_sub_field('button_text')); ?></a> 
                                <div id="pg-developer-modal-<?php echo esc_attr($identifier); ?>" class="modal modal__bg" role="dialog" aria-hidden="true">
                                    <div class="modal__dialog vid">
                                        <div class="modal__content embed-container">

                                            <p><?php esc_attr(the_sub_field('video_link')); ?></p>

                                            <!-- modal close button -->
                                            <a href="" class="modal__close pg-developer-close">
                                                <svg class="pg-developer-mod" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                <?php } else { ?>
                                <a class="btn-link" href="<?php echo esc_url(get_sub_field('button_link')); ?>" ><?php echo esc_attr(get_sub_field('button_text')); ?></a>
                <?php } endif; ?>

                    </div>
                </div>
            </section>


                        <?php
                    //PARALLAX
                    elseif (get_row_layout() == 'parallax'):
                        $banner_img = get_sub_field('image');
                        $imgsize = 'pg-developer-split';
                        $imgsizebot = 'pg-developer-jumbotron';
                        $imgvalues = get_sub_field('side');
                        $identifier = rand(1, 299);
                        ?>
            <section class="pg-developer-parallax parallax-section" <?php if (get_sub_field('id')) { ?>
                         id="<?php echo esc_attr(get_sub_field('id')); ?>"
            <?php } ?> style="background-color:<?php echo esc_attr(get_sub_field('color')); ?>;<?php if (get_sub_field('background') == 'image') { ?>background-image:url('<?php echo esc_attr(get_sub_field('sc_image')); ?>');<?php } ?>">
                <div class="pg-developer-header-overlay"></div>
                <div class="pg-developer-content">
                    <div class="img-holder <?php echo esc_attr($alignment); ?>" data-sr="move 16px scale up 20%, over 2s">
            <?php echo wp_get_attachment_image($banner_img, $imgsize); ?>

                    </div>
                    <div class="text-holder" style="color:<?php echo esc_attr(get_sub_field('textfont_color')); ?>;">
                        <h2 style="color:<?php echo esc_attr(get_sub_field('font_color')); ?>;" ><?php echo esc_attr(get_sub_field('title')); ?></h2>
            <?php if (get_sub_field('sub_title')) : ?>
                            <h3 style="color:<?php echo esc_attr(get_sub_field('subfont_color')); ?>"><?php echo esc_attr(get_sub_field('sub_title')); ?></h3>
            <?php endif; ?>
            <?php echo wp_kses_post(get_sub_field('text')); ?>
            <?php if (get_sub_field('button_text')) :
                if (get_sub_field('video_link')) {
                    ?>
                                <a class="btn-link modal__trigger" data-modal="#pg-developer-modal-<?php echo esc_attr($identifier); ?>" href="" ><?php echo esc_attr(get_sub_field('button_text')); ?></a> 
                                <div id="pg-developer-modal-<?php echo esc_attr($identifier); ?>" class="modal modal__bg" role="dialog" aria-hidden="true">
                                    <div class="modal__dialog vid">
                                        <div class="modal__content embed-container">

                                            <p><?php esc_attr(the_sub_field('video_link')); ?></p>

                                            <!-- modal close button -->
                                            <a href="" class="modal__close pg-developer-close">
                                                <svg class="pg-developer-mod" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            <?php } else { ?>
                                <a class="btn-link" href="<?php echo esc_url(get_sub_field('button_link')); ?>" ><?php echo esc_attr(get_sub_field('button_text')); ?></a>
                                <?php } endif; ?>

                    </div>
                </div>
            </section>

            <?php
        // MINI GALLERY
        elseif (get_row_layout() == 'mini_gallery'):
            ?>
            <section class="pg-developer-single-item <?php if (get_sub_field('position')) {
                echo esc_attr('pg-developer-left');
            } ?>" <?php if (get_sub_field('id')) { ?>
                         id="<?php echo esc_attr(get_sub_field('id')); ?>"
                        <?php } ?>>
                <div class="pg-developer-slider-wrapper">

                            <?php
                        $images = get_sub_field('gallery');

                        if ($images): $g = 1;
                            ?>
                        <ul class="pg-developer-slider">
                <?php foreach ($images as $image): ?>
                                <li <?php if ($g == 1) {
                        echo wp_kses_post('class="selected"');
                    } ?>><img src="<?php echo esc_attr($image['sizes']['pg-developer-minigal']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" /> </li>
                    <?php $g++;
                endforeach; ?>
                        </ul> <!-- pg-developer-slider -->
            <?php endif; ?>


                    <ul class="pg-developer-slider-navigation">
                        <li><a href="#0" class="pg-developer-prev inactive"><?php esc_html_e('Next', 'developer'); ?></a></li>
                        <li><a href="#0" class="pg-developer-next"><?php esc_html_e('Prev', 'developer'); ?></a></li>
                    </ul> <!-- pg-developer-slider-navigation -->

                    <a href="#0" class="pg-developer-close"><?php esc_html_e('Close', 'developer'); ?></a>
                </div> <!-- pg-developer-slider-wrapper -->

                <div class="pg-developer-item-info">
                    <div class="pg-developer-text-wrapper">
                        <h2><?php echo esc_attr(get_sub_field('title')); ?></h2>
                        <h4><?php echo esc_attr(get_sub_field('subtext')); ?></h4>
                    <?php if (get_sub_field('subtitle')) : ?>
                            <p><?php echo wp_kses_post(get_sub_field('subtitle')); ?></p>
            <?php endif; ?>

                        <button class="minigal"><?php esc_html_e('Gallery', 'developer'); ?></button>
            <?php
            if (get_sub_field('add_button')):
                while (has_sub_field('add_button')):
                    ?>
                    <?php if (get_sub_field('link')) { ?>
                                    <a class ="btn-link <?php if (get_sub_field('highlight')) { ?> pg-developer-highlight <?php } ?>" href="<?php echo esc_url(get_sub_field('link')); ?>"><?php echo esc_attr(get_sub_field('text')); ?> </a>
                    <?php } ?>
                <?php endwhile;
            endif; ?>
                    </div>		
                </div> <!-- pg-developer-item-info -->

            </section> <!-- pg-developer-single-item -->


                        <?php
                    // HEADINGS
                    elseif (get_row_layout() == 'heading_block'):
                        $size = get_sub_field('size');
                        $color = get_sub_field('color');
                        $sub_size = get_sub_field('sub_size');
                        $sub_color = get_sub_field('sub_color');
                        $line_size = get_sub_field('line_size');
                        $position = get_sub_field('alignment');
                        $heading_text = get_sub_field('text');
                        $heading_subtext = get_sub_field('subtext');
                        $top = get_sub_field('padding_top');
                        $bottom = get_sub_field('padding_bottom');
                        ?>
            <section <?php if (get_sub_field('id')) { ?>
                    id="<?php echo esc_attr(get_sub_field('id')); ?>"
            <?php } ?> style="background-color:<?php echo esc_attr(get_sub_field('bg_color')); ?>;background-image:url('<?php echo esc_attr(get_sub_field('background_image')); ?>');">
                <div class="white-section br_editor pg-developer-heading" style="text-align:<?php echo esc_attr($position); ?>; padding-top=:<?php echo esc_attr($top); ?>; padding-bottom:<?php echo esc_attr($bottom); ?>; ">
            <?php
            $icona = get_sub_field('icon_upload');

            if (!empty($icona)) {
                ?>

                        <img src="<?php echo esc_url($icona['url']); ?>" alt="<?php echo esc_attr($icona['alt']); ?>" data-sr="move 16px scale up 20%, over 2s">

            <?php } else { ?>
                        <span class="icon fa <?php echo esc_attr(get_sub_field('icon')); ?>"></span>
            <?php } ?>
                    <h2 style="font-size:<?php echo esc_attr($size); ?>px;color:<?php echo esc_attr($color); ?>;"><?php echo esc_attr($heading_text); ?></h2>
                    <span style="font-size:<?php echo esc_attr($sub_size); ?>px;line-height:<?php echo esc_attr($line_size); ?>px;color:<?php echo esc_attr($sub_color); ?>;"><?php echo esc_attr(wp_strip_all_tags($heading_subtext)); ?></span>
                </div>
            </section>
                <?php
            // POI
            elseif (get_row_layout() == 'poi_block'):
                ?>
            <section <?php if (get_sub_field('id')) { ?>
                    id="<?php echo esc_attr(get_sub_field('id')); ?>"
                    <?php } ?> style="background-color:<?php echo esc_attr(get_sub_field('color')); ?>;background-image:url('<?php echo esc_attr(get_sub_field('bg_image')); ?>');">

            <?php
            $poi_img = get_sub_field('image');
            $poiimgsize = 'pg-developer-split';
            ?>
                <div class="pg-developer-content">
                    <div class="pg-developer-product">
                        <div class="pg-developer-product-intro">



                            <h2 style="color:<?php echo esc_attr(get_sub_field('font_color')); ?>;" ><?php echo esc_attr(get_sub_field('title')); ?></h2>
            <?php if (get_sub_field('subtitle')) : ?>
                                <h4 style="color:<?php echo esc_attr(get_sub_field('subfont_color')); ?>"><?php echo esc_attr(get_sub_field('subtitle')); ?></h4>
            <?php endif; ?>
            <?php echo wp_kses_post(get_sub_field('text')); ?>
                            <div class="pg-developer-triggers">
                                <a href="pg-developer-product-tour" class="pg-developer-start btn-link"><?php echo esc_attr(get_sub_field('button')); ?></a>
                            </div>
                        </div>

                        <div id="pg-developer-product-tour" class="pg-developer-product-mockup">
                <?php
                echo wp_get_attachment_image($poi_img, $poiimgsize);
                if (get_sub_field('poi')):
                    ?>
                                <ul class="pg-developer-points-container">
                <?php
                while (has_sub_field('poi')):
                    $dv_left = get_sub_field('left');
                    $dv_top = get_sub_field('top');
                    ?>

                                        <li class="pg-developer-single-point" style="left:<?php echo esc_attr($dv_left); ?>%;top:<?php echo esc_attr($dv_top); ?>%;">
                                            <a class="pg-developer-img-replace" href="#0">More info</a>
                                            <div class="pg-developer-expand__card active">
                                    <?php echo esc_attr(get_sub_field('content')); ?>
                                            </div>
                                        </li> <!-- .pg-developer-single-point -->
                <?php endwhile; ?>
                                </ul> <!-- .pg-developer-points-container -->
            <?php endif; ?>
                            <div class="pg-developer-3d-right-side"></div>
                            <div class="pg-developer-3d-bottom-side"></div>
                        </div> <!-- .pg-developer-product-mockup -->
                        <a href="#0" class="pg-developer-close-product-tour pg-developer-img-replace"></a>
                    </div>
                </div>
            </section> 


                                <?php
                            // TESTIMONIALS

                            elseif (get_row_layout() == 'testimonials_block'):if (get_sub_field('new_testimonial')):
                                    ?>
                <section class="testimonial" <?php if (get_sub_field('id')) { ?>
                             id="<?php echo esc_attr(get_sub_field('id')); ?>"
                                    <?php } ?> style="background-color:<?php echo esc_attr(get_sub_field('background')); ?>;)">

                                <?php
                                while (has_sub_field('new_testimonial')):
                                    $tm_img = get_sub_field('image');
                                    $tmimgsize = 'pg-developer-testimonial';
                                    ?>
                        <div class="inner testimonial">
                            <div class="quote-wrapper">
                                <div class="quote">
                                    <blockquote class="quote-quote"><?php echo esc_attr(get_sub_field('testimonial')); ?></blockquote><cite><?php echo esc_attr(get_sub_field('signature')); ?> <?php if (get_sub_field('company')) { ?>, <?php echo esc_attr(get_sub_field('company'));
                                    } ?> </cite>
                                </div>
                                <div class="portrait" data-sr="move 16px scale up 20%, over 2s"><?php echo wp_get_attachment_image($tm_img, $tmimgsize); ?></div>
                            </div>
                        </div>
                    <?php endwhile;
                ?>


                    <button class="prev-testimonial">Prev</button>
                    <button class="next-testimonial">Next</button>
                </section>
                    <?php endif;
                ?>
                <?php
            // TOGGLE BUTTONS
            elseif (get_row_layout() == 'toggle_block'):if (get_sub_field('buttons')):
                    ?>
                <section class="toggle-btn" <?php if (get_sub_field('id')) { ?>
                             id="<?php echo esc_attr(get_sub_field('id')); ?>"
                <?php } ?> style="background-color:<?php echo esc_attr(get_sub_field('color')); ?>;<?php if (get_sub_field('background') == 'image') { ?>background-image:url('<?php echo esc_attr(get_sub_field('image'));
                } ?>');">
                    <div class="stack-divider">	</div>
                    <div class="buttons-ctn">
                    <?php
                    $button_no = 1;


                    while (has_sub_field('buttons')):
                        if ($button_no == 1) {
                            $btn_class = 'button--left';
                            $content_class = 'button__content--left';
                            $btn_str = 'enter left move 46px scale up 20%, over 1s';
                        } else {
                            $btn_class = 'button--right';
                            $content_class = 'button__content--right';
                            $btn_str = 'enter right move 46px scale up 20%, over 1s';
                        }


                        if (get_sub_field('dv_form') != 'none' && get_sub_field('dv_form') != '') {
                            ?>



                                <button class=" <?php if (get_sub_field('dv_form') != 'none') { ?> open <?php } ?>" style="background-color:<?php echo esc_attr(get_sub_field('background')); ?>;color:<?php echo esc_attr(get_sub_field('font_color')); ?>;"><?php echo esc_attr(get_sub_field('button_label')); ?> </button>


                                <div class="modal-frame">
                                    <div class="modal">
                                        <div class="modal-inset" id="modal-top">
                                            <div class="button close"></div>

                                            <div class="modal-body pg-developer-input">

                                <?php
                                $form = get_sub_field('dv_form');
                                if ($form != 'none') {
                                    echo esc_attr($form);
                                    echo do_shortcode('[contact-form-7 title="' . $form . '"]');
                                    ?>
                        <?php } ?>  
                                            </div>
                                        </div>
                                    </div>
                                </div>




                        <?php
                    } elseif (get_sub_field('video_link')) {
                        ?>

                                <button class="vid open" style="background-color:<?php echo esc_attr(get_sub_field('background')); ?>;color:<?php echo esc_attr(get_sub_field('font_color')); ?>;"><?php echo esc_attr(get_sub_field('button_label')); ?> </button>


                                <div class="modal-frame vid">
                                    <div class="modal">
                                        <div class="modal-inset">
                                            <div class="button close"></div>

                                            <div class="modal-body">

                                                <p><?php esc_attr(the_sub_field('video_link')); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-overlay"></div>

                            <?php } else {
                                ?>

                                <a href="<?php echo esc_url(get_sub_field('button_link')); ?>" class="btn-link" data-sr="<?php echo esc_attr($btn_str); ?>" data-color="<?php echo esc_attr(get_sub_field('background')); ?>" style="background-color:<?php echo esc_attr(get_sub_field('background')); ?>;color:<?php echo esc_attr(get_sub_field('font_color')); ?>;"><span><?php echo esc_attr(get_sub_field('button_label')); ?></span></a>

                    <?php }


                    $button_no++;
                    ?>
                <?php endwhile; ?>

                    </div>

            <?php endif; ?>

            </section>
            <div class="modal-overlay"></div>	
            <?php elseif (get_row_layout() == 'dv_map'):
            ?>
                    <?php if (get_sub_field('markers')): ?>
                <div class="pg-developer-map" style ="height:<?php echo esc_attr(the_sub_field('map_height')); ?>px;" <?php if (get_sub_field('id')) { ?>
                         id="<?php echo esc_attr(get_sub_field('id')); ?>"
                        <?php } ?>>

                <?php
                while (has_sub_field('markers')) :

                    $location = get_sub_field('location');
                    ?>
                        <div class="marker" data-icon="<?php echo esc_attr(the_sub_field('icon')); ?>" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>" data-color="<?php echo esc_attr(the_sub_field('color')); ?>" >
                            <h4><?php esc_attr(the_sub_field('title')); ?></h4>
                            <p class="address"><?php echo esc_attr($location['address']); ?></p>
                    <?php esc_attr(the_sub_field('text')); ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
            <?php $dv_api = esc_attr(get_theme_mod('gmap')); ?>
            <?php if (get_theme_mod('gmap')) { ?>

                <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $dv_api ?>&callback=initMap"></script>
            <?php } else { ?>
                <script src="https://maps.googleapis.com/maps/api/js"></script>
                 <?php } ?>
            <script type="text/javascript">
                (function ($) {

                    /*
                     *  render_map
                     *
                     *  This function will render a Google Map onto the selected jQuery element
                     *
                     *  @type	function
                     *  @date	8/11/2013
                     *  @since	4.3.0
                     *
                     *  @param	$el (jQuery element)
                     *  @return	n/a
                     */

                    function render_map($el) {

            // var
                        var $markers = $el.find('.marker');
                        var dragging = true;

                        if (Modernizr.touch) {
                            dragging = false;
                        }
            // vars
                        var args = {
                            zoom: 16,
                            draggable: dragging,
                            scrollwheel: false,
                            center: new google.maps.LatLng(0, 0),
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            styles: [{"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#d6f3f9"}, {"lightness": 17}]}, {"featureType": "landscape", "elementType": "geometry", "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]}, {"featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{"color": "#ffffff"}, {"lightness": 17}]}, {"featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]}, {"featureType": "road.arterial", "elementType": "geometry", "stylers": [{"color": "#ffffff"}, {"lightness": 18}]}, {"featureType": "road.local", "elementType": "geometry", "stylers": [{"color": "#ffffff"}, {"lightness": 16}]}, {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]}, {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#dedede"}, {"lightness": 21}]}, {"elementType": "labels.text.stroke", "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]}, {"elementType": "labels.text.fill", "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]}, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "transit", "elementType": "geometry", "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]}, {"featureType": "administrative", "elementType": "geometry.fill", "stylers": [{"color": "#fefefe"}, {"lightness": 20}]}, {"featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]}]

                        };

            // create map	        	
                        var map = new google.maps.Map($el[0], args);

            // add a markers reference
                        map.markers = [];




            // add markers
                        $markers.each(function () {

                            add_marker($(this), map);

                        });

            // center map
                        center_map(map);


                    }

            // create info window outside of each - then tell that singular infowindow to swap content based on click
                    var infowindow = new google.maps.InfoWindow({
                        content: ''
                    });

                    /*
                     *  add_marker
                     *
                     *  This function will add a marker to the selected Google Map
                     *
                     *  @type	function
                     *  @date	8/11/2013
                     *  @since	4.3.0
                     *
                     *  @param	$marker (jQuery element)
                     *  @param	map (Google Map object)
                     *  @return	n/a
                     */

                    function add_marker($marker, map) {

            // var
                        var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));
                        var color = $marker.attr('data-color');



                        var mark_icon = $marker.attr('data-icon');
                        if (mark_icon === 'police') {
                            var mark = police;
                        }
                        else if (mark_icon === 'university') {
                            var mark = university;
                        }
                        else if (mark_icon === 'pin') {
                            var mark = pin;
                        }
                        else if (mark_icon === 'airport') {
                            var mark = airport;
                        }
                        else if (mark_icon === 'bus') {
                            var mark = bus;
                        }
                        else if (mark_icon === 'fire') {
                            var mark = fire;
                        }
                        else if (mark_icon === 'grocery') {
                            var mark = grocery;
                        }
                        else if (mark_icon === 'gym') {
                            var mark = gym;
                        }
                        else if (mark_icon === 'hospital') {
                            var mark = hospital;
                        }
                        else if (mark_icon === 'restaurant') {
                            var mark = restaurant;
                        }
                        else if (mark_icon === 'school') {
                            var mark = school;
                        }
                        else if (mark_icon === 'shopping') {
                            var mark = shopping;
                        }
                        else if (mark_icon === 'theatre') {
                            var mark = theatre;
                        }
                        else if (mark_icon === 'train') {
                            var mark = train;
                        }
                        else if (mark_icon === 'realestate') {
                            var mark = realestate;
                        }

                        var icon = {
                            path: mark,
                            fillColor: color,
                            fillOpacity: .8,
                            strokeWeight: 0,
                            scale: .6
                        }


            // create marker
                        var marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            icon: icon,
                        });


            // add to array
                        map.markers.push(marker);

            // if marker contains HTML, add it to an infoWindow
                        if ($marker.html())
                        {




                            // show info window when marker is clicked & close other markers
                            google.maps.event.addListener(marker, 'click', function () {
                                //swap content of that singular infowindow
                                infowindow.setContent($marker.html());
                                infowindow.open(map, marker);
                            });

                            // close info window when map is clicked
                            google.maps.event.addListener(map, 'click', function (event) {
                                if (infowindow) {
                                    infowindow.close();
                                }
                            });


                        }

                    }

                    /*
                     *  center_map
                     *
                     *  This function will center the map, showing all markers attached to this map
                     *
                     *  @type	function
                     *  @date	8/11/2013
                     *  @since	4.3.0
                     *
                     *  @param	map (Google Map object)
                     *  @return	n/a
                     */

                    function center_map(map) {

            // vars
                        var bounds = new google.maps.LatLngBounds();

            // loop through all markers and create bounds
                        $.each(map.markers, function (i, marker) {

                            var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

                            bounds.extend(latlng);

                        });

            // only 1 marker?
                        if (map.markers.length == 1)
                        {
                            // set center of map
                            map.setCenter(bounds.getCenter());
                            map.setZoom(16);
                        }
                        else
                        {
                            // fit to bounds
                            map.fitBounds(bounds);
                        }

                    }

                    /*
                     *  document ready
                     *
                     *  This function will render each map when the document is ready (page has loaded)
                     *
                     *  @type	function
                     *  @date	8/11/2013
                     *  @since	5.0.0
                     *
                     *  @param	n/a
                     *  @return	n/a
                     */

                    $(document).ready(function () {

                        $('.pg-developer-map').each(function () {

                            render_map($(this));

                        });

                    });

                })(jQuery);


            </script>

            <?php endif;
        ?>
        <?php
    endwhile;
else :
// no layouts found
endif;
?> 