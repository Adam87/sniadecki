<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Developer
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="pg-developer-content">


        <p class="text">

            <?php the_content(); ?>
    </div>

    <footer class="entry-footer">
        <?php dv_entry_footer(); ?>
        <div class="pg-developer-social">
            <nav class="pg-developer-social-menu">

                <a href="https://twitter.com/share?url=<?php echo esc_url(get_the_permalink()); ?>" target="_blank" class="pg-developer-social-menu-item"> <i class="fa fa-twitter"></i> </a>
                <a href="https://www.facebook.com/sharer.php?u=<?php echo esc_url(get_the_permalink()); ?>" target="_blank" class="pg-developer-social-menu-item"> <i class="fa fa-facebook"></i> </a>

                <a href="https://plus.google.com/share?url=<?php echo esc_url(get_the_permalink()); ?>" target="_blank" class="pg-developer-social-menu-item"> <i class="fa fa-google-plus"></i> </a>

            </nav>

        </div>
    </footer><!-- .entry-footer -->

    <?php
    global $user_ID;
    if (get_the_author_meta('description')) {
        ?>
        <div class="pg-developer-content author-bio">
            <div class="avatar-holder">
                <?php echo get_avatar(get_the_author_meta('email'), '60'); ?>
            </div>
            <div class="commentlist-holder">
                <span class="name"><?php the_author_link(); ?></span>
                <ul class="bio-icons">
                    <?php
                    $rss_url = get_the_author_meta('rss_url');
                    if ($rss_url && $rss_url != '') {
                        ?>
                        <li class="rss"><a href="'.esc_url($rss_url).'"><i class="fa fa-rss"></i></a></li>
                    <?php
                    }

                    $google_profile = get_the_author_meta('google_profile');
                    if ($google_profile && $google_profile != '') {
                        ?>
                        <li class="google"><a href="'.esc_url($google_profile).'" rel="author"><i class="fa fa-google"></i></a></li>
                    <?php
                    }

                    $twitter_profile = get_the_author_meta('twitter_profile');
                    if ($twitter_profile && $twitter_profile != '') {
                        ?>
                        <li class="twitter"><a href="'.esc_url($twitter_profile).'"><i class="fa fa-twitter"></i></a></li>
                    <?php
                    }

                    $facebook_profile = get_the_author_meta('facebook_profile');
                    if ($facebook_profile && $facebook_profile != '') {
                        ?>
                        <li class="facebook"><a href="'.esc_url($facebook_profile).'"><i class="fa fa-facebook"></i></a></li>
    <?php
    }

    $linkedin_profile = get_the_author_meta('linkedin_profile');
    if ($linkedin_profile && $linkedin_profile != '') {
        ?>
                        <li class="linkedin"><a href="'.esc_url($linkedin_profile).'"><i class="fa fa-linkedin"></i></a></li>
        <?php }
        ?>
                </ul>
                <p><?php the_author_meta('description'); ?></p>



            </div>
        </div>
        <!--END .author-bio-->
<?php } ?>

<?php
wp_link_pages(array(
    'before' => '<div class="page-links">' . esc_html__('Pages:', 'developer'),
    'after' => '</div>',
));
?>

</article><!-- #post-## -->


