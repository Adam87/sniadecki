<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Developer
 */
get_header();
?>
<div class="header-banner-top">
    <div class="banner">
        <?php if (has_post_format('gallery')) {
            if (get_field('post_gallery')):
                ?>
                <div class="slide">

                    <ul>
                        <?php $images = get_field('post_gallery'); ?>

                        <?php
                        foreach ($images as $image):
                            ?>
                            <li style="background-image:url(<?php echo esc_url($image['url']); ?>);">

                            </li>
                            <?php endforeach; ?>
                    </ul>
                </div>


            <?php
            endif;
        }
        elseif (has_post_thumbnail()) {
            $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'pg-developer-header');
            $url = $thumb['0'];
            ?>

            <div class="banner-image" style="background-image:url(<?php echo esc_url($url); ?>);"></div>
        <?php } ?>
        <div class="pg-developer-header-overlay"></div>
        <div class="primary-wrapper">

            <?php the_title('<h1 class="entry-title">', '</h1>'); ?>

            <p class="byline date">
                <?php dv_posted_on(); ?>
            </p><!-- .entry-meta -->
        </div>

    </div>
</div>

<?php if (function_exists('get_field') && get_field('select_sidebar') == "fullwidth") { ?>
    <div class="full-width">
    <?php } else { ?>

    <?php } ?>
    <div class="pg-developer-holder">
        <div class="content">

            <div id="primary" class="content-area">
                <main id="main" class="site-main" role="main">

                    <?php while (have_posts()) : the_post(); ?>

                        <?php get_template_part('template-parts/content', 'single'); ?>
                        <div class="pg-developer-content">
                            <?php dv_post_nav(); ?>
                        </div>
                        <?php
                        // If comments are open or we have at least one comment, load up the comment template.
                        if (comments_open() || get_comments_number()) :
                            comments_template();
                        endif;
                        ?>

                    <?php endwhile; // End of the loop. ?>

                </main><!-- #main -->
            </div><!-- #primary -->


            <?php if (function_exists('get_field') && get_field('select_sidebar') == "fullwidth") { ?>
            </div>
        <?php
        } else {
            get_sidebar();
            ?>


<?php } ?>
    </div></div>	
<?php get_footer(); ?>
